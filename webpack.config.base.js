const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ProvidePlugin = require('webpack/lib/ProvidePlugin');
const NoEmitOnErrorsPlugin = require('webpack/lib/NoEmitOnErrorsPlugin');

const ASSETS_PATH = 'assets/';

// Each instance will generate a separate resource
const extractTextAppStyles = new ExtractTextPlugin({
  filename: `${ASSETS_PATH}app.[contenthash].css`,
  allChunks: true
});
const extractTextVendorStyles = new ExtractTextPlugin(
  `${ASSETS_PATH}vendor.[contenthash].css`
);


const config = {
  // main and vendor resources generated
  entry: {
    index: './src/index.js',
    vendors: [
      'bootstrap/dist/js/bootstrap.min',
      'd3/d3.min.js',
      'jquery/dist/jquery.min',
      'jquery.easing/jquery.easing.min',
      'magnific-popup/dist/jquery.magnific-popup.min',
      'popper.js/dist/umd/popper-utils.min',
      'prop-types',
      'react',
      'react-dom',
      'react-redux',
      'react-router-dom',
      'redux',
      'scrollreveal/dist/scrollreveal.min',
      'tether/dist/js/tether.min',
      'topojson/build/topojson.min'
    ],
    resources: [
      'world-tour/core-map.js',
      'assets/resources/cities.json',
      'assets/resources/world-110m.json',
      'assets/resources/world-country-names.json'
    ]
  },

  resolve: {
    modules: ['node_modules', 'src'],
    extensions: ['.js', '.jsx']
  },

  // Exclude external testing stuff from bundles
  externals: {
    cheerio: 'window',
    'react/addons': true,
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true,
  },

  plugins: [
    // Dynamically generate index.html and use the html-webpack-template for
    // app binding <div id="reactApp"></div>.
    new HtmlWebpackPlugin({
      inject: 'head',
      template: require('html-webpack-template'),
      title: 'hohmanjl/d3demo'
    }),

    // The external style sheets
    extractTextVendorStyles,
    extractTextAppStyles,

    // This is required so bootstrap can bootstrap. If not included,
    // bootstrap complains about missing jQuery.
    new ProvidePlugin({
      jQuery: 'jquery/dist/jquery.min',
      $: 'jquery/dist/jquery.min',
      jquery: 'jquery/dist/jquery.min',
      Popper: ['popper.js/dist/umd/popper-utils.min', 'default'],
      Tether: 'tether/dist/js/tether.min'
    }),

    new NoEmitOnErrorsPlugin()
  ],

  module: {
    rules: [
      {
        // compile the less via less-loader, pass result to css-loader
        // use sourcemaps for easy debugging
        test: /\.less$/,
        use: extractTextAppStyles.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                sourceMap: true
              }
            },
            {
              loader: 'less-loader',
              options: {
                sourceMap: true
              }
            }
          ]
        })
      },
      {
        // compile css resources as vendor css with sourcemaps for debugging
        test: /\.css$/,
        use: extractTextVendorStyles.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                sourceMap: true
              }
            }
          ]
        })
      },
      {
        // compile javascript via eslint loader then pass result to babel loader
        test: /\.jsx?$/,
        exclude: [
          /node_modules/
        ],
        use: [
          'babel-loader',
        ]
      },
      {
        // process small assets < 8192 bytes as data urls,
        // assets > 8192 bytes processed by file-loader (logic in the url-loader)
        test: /\.(woff2?|ttf|eot|svg|png|jpg|jpeg)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192
            }
          }
        ]
      }
    ]
  }
};


module.exports = {
  config,
  ASSETS_PATH
};
