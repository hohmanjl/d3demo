# d3demo
---

By: James Hohman

## About

This is a modification of
[Mike Bostock's "world tour"](http://bl.ocks.org/mbostock/4183330)--A
topographical d3.js canvas animation of the world map as an
orthographic projection. I modified the canvas animation to be an
SVG animation. While the canvas element offers superior rendering
performance and frame rate, it lacks interactive features. I added a
few landmark cities and some hover effects.

## Live Demo

http://jhohman-d3demo.surge.sh/
