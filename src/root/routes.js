const svg = {
  name: 'svg',
  url: '/world-tour/svg/'
};

const canvas = {
  name: 'canvas',
  url: '/world-tour/canvas/'
};

const routes = {
  svg,
  canvas,
};

export default routes;
