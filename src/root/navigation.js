'use strict';

import React, { PureComponent } from 'react';
import { HashLink as Link } from 'react-router-hash-link';
import PropTypes from 'prop-types';
import routes from 'root/routes';

const NAV_LINKS = [routes.svg, routes.canvas];


class Navigation extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <nav
        className="navbar navbar-expand-lg navbar-light fixed-top"
        id="mainNav">
        <div className="container">
          <Link
            className="navbar-brand js-scroll-trigger"
            to="/">
            { this.props.appTitle }
          </Link>
          <button
            className="navbar-toggler navbar-toggler-right"
            type="button"
            data-toggle="collapse"
            data-target="#navbarResponsive"
            aria-controls="navbarResponsive"
            aria-expanded="false"
            aria-label="Toggle navigation">
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarResponsive">
            <ul className="navbar-nav ml-auto">
              {
                NAV_LINKS.map((config, i) => {
                  return (
                    <li key={i} className="nav-item">
                      <Link
                        className="nav-link js-scroll-trigger"
                        to={ config.url }>
                        { config.name }
                      </Link>
                    </li>
                  );
                })
              }

              <li className="nav-item">
                <Link
                  className="nav-link js-scroll-trigger"
                  to="#about">
                  About
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className="nav-link js-scroll-trigger"
                  to="#portfolio">
                  Portfolio
                </Link>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link js-scroll-trigger"
                  href="https://bitbucket.org/hohmanjl/d3demo/"
                  target="_blank">
                  Repo
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  };
}

Navigation.defaultProps = {
  appTitle: 'It worked!'
};

Navigation.propTypes = {
  appTitle: PropTypes.string
};

export default Navigation;
