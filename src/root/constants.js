'use strict';

const APP_TITLE = 'hohmanjl/d3demo';
const MAP_TARGET_ID = 'map-target';

export { APP_TITLE, MAP_TARGET_ID };
