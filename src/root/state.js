'use strict';

import { combineReducers } from 'redux';
import { animationSpeedReducer } from 'world-tour/state';


const rootReducer = combineReducers({
  global: (state = []) => state,
  speed: animationSpeedReducer
});


export default rootReducer;
