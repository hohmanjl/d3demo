'use strict';

function benchmark(callback, iterations) {
  let times = [];
  for (let i = 0; i < iterations; i++) {
    let start = performance.now();
    callback();
    let end = performance.now();
    let elapsed = end - start;

    times.push(elapsed);
  }

  let min = Math.min(...times).toPrecision(3);
  let max = Math.max(...times).toPrecision(3);
  let avg = (times.reduce((acc, cur) => {
    return acc + cur;
  }, 0) / times.length).toPrecision(3);

  console.log(times.map((e) => { return e.toPrecision(3); }));
  console.log(
    `benchmark "${callback.name}": ` +
    `${iterations} iterations (${min}, ${max}, ${avg})ms min/max/avg`
  );
}

export benchmark;
