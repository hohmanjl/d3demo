// Load vendors first
import 'popper.js/dist/umd/popper-utils.min';
import 'magnific-popup/dist/jquery.magnific-popup.min';
import 'jquery.easing/jquery.easing.min';
import 'scrollreveal/dist/scrollreveal.min';
import 'bootstrap/dist/js/bootstrap.min';

import 'magnific-popup/dist/magnific-popup.css';
import 'tether/dist/css/tether.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';

// Load app stuff later
import 'assets/creative/css/creative.css';
import 'assets/less/global-styles.less';

import React from 'react';
import { render } from 'react-dom';

import App from './App';

render(<App />, document.body);
