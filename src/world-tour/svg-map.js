/**
 * Created by James on 5/15/2016.
 *
 * Original: Mike Bostock
 * Source: http://bl.ocks.org/mbostock/4183330
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import d3 from 'd3/d3.min';
import CoreMap from './core-map';
import './svg-map.less';
import { eulerAngles } from './map-utils';


class SvgMap extends CoreMap {
  constructor(containerId, animationSpeed) {
    super(containerId, animationSpeed);
    // this.config.animate = false;
  }

  drawChart() {
    const chart = this.chart;
    const cityData = this.constructor.cities;
    const countries = this.constructor.countries;
    const globe = this.constructor.globe;

    chart.graticule = d3.geo.graticule();

    chart.path = d3.geo.path()
      .projection(chart.projection);

    chart.svg.append('path')
      .datum(globe)
      .attr('class', 'ocean')
      .attr('d', chart.path);

    countries.forEach((o) => {
      chart.svg.append('path')
        .datum(o)
        .attr('class', 'country')
        .attr('d', chart.path)
        .append('svg:title')
        .text(d => d.name);
    });

    chart.svg.append('path')
      .datum(globe)
      .attr('class', 'globe')
      .attr('d', chart.path);

    chart.svg.append('path')
      .datum(chart.graticule)
      .attr('class', 'graticule')
      .attr('d', chart.path);

    const cities = chart.svg.selectAll('g')
      .data(cityData)
      .enter()
      .append('g')
      .attr('class', 'city');

    cities.append('path')
      .datum((d) => {
        return {
          type: 'Point',
          coordinates: [d.lng, d.lat],
          radius: 3,
          data: d
        };
      })
      .attr('class', 'city-marker')
      .attr('id', d => d.data.id)
      .attr('d', chart.path);

    cities.append('text')
      .attr('transform', (d) => {
        return `translate(${chart.projection([d.lng, d.lat])})`;
      })
      .attr('dy', 10)
      .attr('dx', 8)
      .text(d => d.name)
      .attr('visibility', (d) => {
        return d3.select(`#${d.id}`)
          .attr('d') !== null ? 'visible' : 'hidden';
      });
  }

  zoomed() {
    const chart = this.chart;
    chart.scaleFactor = d3.event.scale;
    const scale = chart.originalScale * chart.scaleFactor;

    chart.projection.scale(scale);

    chart.svg.selectAll('path')
      .attr('d', chart.path);

    chart.svg.selectAll('text')
      .attr('transform', (d) => {
        return `translate(${chart.projection([d.lng, d.lat])})`;
      })
      .attr('visibility', (d) => {
        return d3.select(`#${d.id}`)
          .attr('d') !== null ? 'visible' : 'hidden';
      });

  }

  dragstarted() {
    const chart = this.chart;
    const gpos0 = chart.projection.invert(d3.mouse(chart.svg.node()));

    if (this.checkNaN(gpos0) === null) return;
    d3.selectAll('*').transition();

    chart.gpos0 = gpos0;

    chart.svg.insert('path')
      .datum({ type: 'Point', coordinates: gpos0 })
      .attr('class', 'drag-point')
      .attr('d', chart.path);
  }

  dragged() {
    const chart = this.chart;
    const gpos1 = chart.projection.invert(d3.mouse(chart.svg.node()));
    const o0 = chart.projection.rotate();

    if (this.checkNaN(o0) === null || this.checkNaN(gpos1) === null) return;

    const o1 = eulerAngles(chart.gpos0, gpos1, o0);

    if (this.checkNaN(o1) === null) return;

    chart.projection.rotate(o1);

    chart.svg.selectAll('.drag-point')
      .datum({ type: 'Point', coordinates: gpos1 });

    chart.svg.selectAll('path')
      .attr('d', chart.path);

    chart.svg.selectAll('text')
      .attr('transform', (d) => {
        return `translate(${chart.projection([d.lng, d.lat])})`;
      })
      .attr('visibility', (d) => {
        return d3.select(`#${d.id}`)
          .attr('d') !== null ? 'visible' : 'hidden';
      });

  }

  dragended() {
    d3.selectAll('.drag-point').remove();

    if (this.config.animate) {
      this.transitionFn(this.constructor.numCountries - 1, this.chart.index - 1);
    }
  }

  ready() {
    const width = this.dims.w;
    const height = this.dims.h;
    const chart = this.chart;
    const drag = d3.behavior.drag()
      .on('dragstart', this.dragstarted.bind(this))
      .on('drag', this.dragged.bind(this))
      .on('dragend', this.dragended.bind(this));
    const zoom = d3.behavior.zoom()
      .scaleExtent([1, 4])
      .on('zoom', this.zoomed.bind(this));

    chart.svg = d3.select(`#${ this.containerId }`).append('svg')
      .attr('class', 'theme-dark')
      .attr('width', width)
      .attr('height', height);

    chart.svg.call(drag);
    chart.svg.call(zoom);

    this.drawChart();

    if (this.config.animate) {
      this.transitionFn(this.constructor.numCountries - 1);
    }
  }

  resize() {
    this.init();
    const width = this.dims.w;
    const height = this.dims.h;
    const chart = this.chart;

    chart.svg
      .attr('width', width)
      .attr('height', height);

    this.updateChart();
  }

  transitionFn(n = 0, i = -1) {
    const countries = this.constructor.countries;
    const chart = this.chart;

    i = i < n ? i + 1 : 0;

    this.chart.country = countries[i];
    this.chart.index = i;
    this.title.text(this.chart.country.name);

    d3.transition()
      .duration(this.animationSpeed)
      .tween('rotate', () => {
        const p = d3.geo.centroid(this.chart.country);
        const rFn = d3.interpolate(
          chart.projection.rotate(),
          [-p[0], -p[1]]
        );

        return this.tweenFn.bind(this, rFn);
      })
      .transition()
      .each(
        'end',
        this.transitionFn.bind(this, n, i)
      );
  }

  tweenFn(rFn, t) {
    const chart = this.chart;

    chart.projection.rotate(rFn(t));

    this.updateChart();
  }

  updateChart() {
    const chart = this.chart;

    chart.svg.selectAll('path.country')
      .attr('d', chart.path)
      .attr('class', (d) => {
        return d.name === chart.country.name
          ? 'highlighted-country country'
          : 'country';
      });

    chart.svg.selectAll('path:not(.country)')
      .attr('d', chart.path);

    chart.svg.selectAll('text')
      .attr('transform', (d) => {
        return `translate(${chart.projection([d.lng, d.lat])})`;
      })
      .attr('visibility', (d) => {
        const selection = d3.select(`#${d.id}`);
        if (selection.empty()) {
          return 'hidden';
        }

        return selection
          .attr('d') !== null ? 'visible' : 'hidden';
      });
  }
}


class SvgApp extends React.Component {
  constructor(props) {
    super(props);

    this.state = this.props.state;

    this.svgMap = new SvgMap(props.mapTarget, this.state.speed);
  }

  componentDidMount() {
    this.svgMap.init();
    this.svgMap.ready();

    window.addEventListener('resize', this.svgMap.resize.bind(this.svgMap));
  }

  componentWillUnmount() {
    this.svgMap.chart.svg.remove();

    window.removeEventListener('resize', this.svgMap.resize.bind(this.svgMap));
  }

  render() {
    return (
      <div className='header-content theme-dark'>
        <div className='header-content-inner'>
          <h1 id='homeHeading'>
            --
          </h1>
        </div>
      </div>
    );
  }
}


SvgApp.propTypes = {
  mapTarget: PropTypes.string.isRequired,
  animate: PropTypes.bool,
};

SvgApp.defaultProps = {
  animate: true,
};

const mapStateToProps = state => {
  return {
    state: state
  };
};


const ConnectedSvgApp = connect(
  mapStateToProps
)(SvgApp);


export default ConnectedSvgApp;
