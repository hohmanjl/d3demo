'use strict';

const SET_ANIMATION_SPEED = Symbol('set-animation-speed');


function setAnimationSpeed(value) {
  return {
    type: SET_ANIMATION_SPEED,
    value
  };
}


function animationSpeedReducer(state = 2500, action) {
  switch (action.type) {
    case SET_ANIMATION_SPEED:
      return action.value;
    default:
      return state;
  }
}



export {
  setAnimationSpeed,
  animationSpeedReducer,
  SET_ANIMATION_SPEED
};
export default animationSpeedReducer;
