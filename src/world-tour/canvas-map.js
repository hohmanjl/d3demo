/**
 * Created by James on 5/15/2016.
 *
 * Original: Mike Bostock
 * Source: http://bl.ocks.org/mbostock/4183330
 */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import d3 from 'd3/d3.min';
import CoreMap from './core-map';


class CanvasMap extends CoreMap {
  constructor(containerId, animationSpeed) {
    super(containerId, animationSpeed);
  }

  drawChart() {
    const chart = this.chart;
    const borders = this.constructor.borders;
    const land = this.constructor.land;
    const globe = this.constructor.globe;
    const width = this.dims.w;
    const height = this.dims.h;
    const context = chart.context;
    const path = chart.path;

    context.clearRect(0, 0, width, height);

    context.globalAlpha = 0.9;
    context.fillStyle = 'royalblue';
    context.beginPath();
    path(globe);
    context.stroke();
    context.fill();

    context.globalAlpha = 1;
    context.fillStyle = '#ccc';
    context.beginPath();
    path(land);
    context.fill();

    context.fillStyle = '#f00';
    context.beginPath();
    path(this.chart.country);
    context.fill();

    context.strokeStyle = '#fff';
    context.lineWidth = .5;
    context.beginPath();
    path(borders);
    context.stroke();

    // chart.cities
    // // FixMe: how do we test for visibility?
    // //   .filter((c) => {
    // //   const geoangle = d3.geo.distance(
    // //     [ c.x, c.y ],
    // //     [
    // //       -chart.projection.rotate()[0],
    // //       chart.projection.rotate()[1]
    // //     ]);
    // //
    // //   return geoangle > half_pi;
    // // })
    //   .map((c) => {
    //     context.strokeStyle = 'black';
    //     context.fillStyle = 'black';
    //     context.fillRect(c.x, c.y, 4, 4);
    //   });

    context.strokeStyle = '#000';
    context.lineWidth = 2;
    context.beginPath();
    path(globe);
    context.stroke();
  }

  ready() {
    const width = this.dims.w;
    const height = this.dims.h;
    const chart = this.chart;

    chart.canvas = d3.select(`#${ this.containerId }`).append('canvas')
      .attr('width', width)
      .attr('height', height);

    chart.context = chart.canvas.node().getContext('2d');

    chart.path = d3.geo.path()
      .projection(chart.projection)
      .context(chart.context);

    this.transitionFn(this.constructor.numCountries - 1);
  }

  resize() {
    this.init();
    const width = this.dims.w;
    const height = this.dims.h;
    const chart = this.chart;

    chart.canvas
      .attr('width', width)
      .attr('height', height);

    this.drawChart();
  }

  transitionFn(n = 0, i = -1) {
    const countries = this.constructor.countries;
    const chart = this.chart;

    i = i < n ? i + 1 : 0;

    this.chart.country = countries[i];
    this.title.text(this.chart.country.name);

    d3.transition()
      .duration(this.animationSpeed)
      .tween('rotate', () => {
        const p = d3.geo.centroid(this.chart.country);
        const rFn = d3.interpolate(
          chart.projection.rotate(),
          [-p[0], -p[1]]
        );

        return this.tweenFn.bind(this, rFn);
      })
      .transition()
      .each(
        'end',
        this.transitionFn.bind(this, n, i)
      );
  }

  tweenFn(rFn, t) {
    const chart = this.chart;

    chart.projection.rotate(rFn(t));

    this.drawChart();
  }
}


class CanvasApp extends PureComponent {
  constructor(props) {
    super(props);

    this.state = this.props.state;

    if (!this.state) {
      this.state = {
        speed: 2500,
      }
    }

    this.canvasMap = new CanvasMap(props.mapTarget, this.state.speed);
  }

  componentDidMount() {
    this.canvasMap.init();
    this.canvasMap.ready();
    window.addEventListener(
      'resize',
      this.canvasMap.resize.bind(this.canvasMap)
    );
  }

  componentWillUnmount() {
    this.canvasMap.chart.canvas.remove();

    window.removeEventListener(
      'resize',
      this.canvasMap.resize.bind(this.canvasMap)
    );
  }

  render() {
    return (
      <div className='header-content'>
        <div className='header-content-inner'>
          <h1 id='homeHeading'>
            --
          </h1>
          <div id='svg-panel'/>
        </div>
      </div>
    );
  }
}


CanvasApp.propTypes = {
  mapTarget: PropTypes.string.isRequired
};


// const mapStateToProps = state => {
//   return {
//     state: state
//   };
// };
//
//
// const ConnectedCanvasApp = connect(
//   mapStateToProps
// )(CanvasApp);


export default CanvasApp;
