'use strict';

import d3 from 'd3/d3.min';

import CITIES from 'assets/resources/cities.json';
import WORLD from 'assets/resources/world-110m.json';
import NAMES from 'assets/resources/world-country-names.json';
const TOPOJSON = require('topojson/build/topojson.min');

const GLOBE = { type: 'Sphere' };
const COUNTRY_MAP = NAMES.reduce((acc, country) => {
  acc[+country.id] = {
    name: country.name || country.sortName,
    sortName: country.sortName
  };
  return acc;
}, {});
const LAND = TOPOJSON.feature(WORLD, WORLD.objects.land);
const ALL_COUNTRIES = TOPOJSON
  .feature(WORLD, WORLD.objects.countries)
  .features;
const BORDERS = TOPOJSON.mesh(
  WORLD,
  WORLD.objects.countries,
  (a, b) => { return a !== b; }
);

function getCountries(countries, countryMap) {
  return countries.filter((datum) => {
    let countryName = countryMap[datum.id];

    if (countryName) {
      Object.assign(datum, countryName);
    }

    return !!countryName;
  }).sort((a, b) => {
    return a.sortName.localeCompare(b.sortName);
  });
}

const COUNTRIES = getCountries(ALL_COUNTRIES, COUNTRY_MAP);
const NUM_CONTRIES = COUNTRIES.length;


export default class CoreMap {
  constructor(containerId, animationSpeed = 5000) {
    if (!containerId) {
      throw new Error('Valid containerId required.');
    }

    this.containerId = containerId;
    this.animationSpeed = animationSpeed;
    this.dims = {};
    this.title = null;
    this.chart = {
      scaleFactor: null,
      country: COUNTRIES[0],
      index: 0
    };
    this.config = {
      animate: true
    };
  }

  static get borders() {
    return BORDERS;
  }

  static get cities() {
    return CITIES;
  }

  static get countries() {
    return COUNTRIES;
  }

  static get countryMap() {
    return COUNTRY_MAP;
  }

  static get globe() {
    return GLOBE;
  }

  static get land() {
    return LAND;
  }

  static get names() {
    return NAMES;
  }

  static get numCountries() {
    return NUM_CONTRIES;
  }

  static get topojson() {
    return TOPOJSON;
  }

  static get world() {
    return WORLD;
  }

  checkNaN(coords) {
    if (coords === null || coords === undefined) {
      return null;
    }

    let hasNaN = false;
    coords.some((c) => {
      if (isNaN(c)) {
        hasNaN = true;
        return true;
      }
    });

    if (hasNaN) return null;

    return coords;
  }

  geometry() {
    let w = $(`#${ this.containerId }`).innerWidth();
    let wh = $(window).height();
    let dim = Math.min(w, wh);

    return { w: w | 0, h: dim | 0 };
  }

  init() {
    this.dims = this.geometry();
    this.title = d3.select('h1');

    const width = this.dims.w;
    const height = this.dims.h;
    const chart = this.chart;

    chart.scaleFactor = chart.scaleFactor === null ? 1 : chart.scaleFactor;
    chart.originalScale = height / 2 - 20;

    if (chart.projection) {
      // If the projection exists update it.
      chart.projection
        .translate([width / 2, height / 2])
        .scale(chart.originalScale * chart.scaleFactor);
    } else {
      chart.projection = d3.geo.orthographic()
        .translate([width / 2, height / 2])
        .scale(chart.originalScale * chart.scaleFactor)
        .clipAngle(90)
        .precision(0.6);
    }
  }
}
