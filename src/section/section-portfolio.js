import React from 'react';

const SectionPortfolio = () => {
  return (
    <section className="p-0" id="portfolio">
      <div className="container-fluid">
        <div className="row no-gutter popup-gallery">
          <div className="col-lg-4 col-sm-6">
            <a className="portfolio-box" href={require('section/assets/images/1.jpg')}>
              <img className="img-fluid" src={require('section/assets/images/1.jpg')} alt="" />
              <div className="portfolio-box-caption">
                <div className="portfolio-box-caption-content">
                  <div className="project-category text-faded">
                    Category
                  </div>
                  <div className="project-name">
                    Project Name
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div className="col-lg-4 col-sm-6">
            <a className="portfolio-box" href={require('section/assets/images/2.jpg')}>
              <img className="img-fluid" src={require('section/assets/images/2.jpg')} alt="" />
              <div className="portfolio-box-caption">
                <div className="portfolio-box-caption-content">
                  <div className="project-category text-faded">
                    Category
                  </div>
                  <div className="project-name">
                    Project Name
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div className="col-lg-4 col-sm-6">
            <a className="portfolio-box" href={require('section/assets/images/3.jpg')}>
              <img className="img-fluid" src={require('section/assets/images/3.jpg')} alt="" />
              <div className="portfolio-box-caption">
                <div className="portfolio-box-caption-content">
                  <div className="project-category text-faded">
                    Category
                  </div>
                  <div className="project-name">
                    Project Name
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div className="col-lg-4 col-sm-6">
            <a className="portfolio-box" href={require('section/assets/images/4.jpg')}>
              <img className="img-fluid" src={require('section/assets/images/4.jpg')} alt="" />
              <div className="portfolio-box-caption">
                <div className="portfolio-box-caption-content">
                  <div className="project-category text-faded">
                    Category
                  </div>
                  <div className="project-name">
                    Project Name
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div className="col-lg-4 col-sm-6">
            <a className="portfolio-box" href={require('section/assets/images/5.jpg')}>
              <img className="img-fluid" src={require('section/assets/images/5.jpg')} alt="" />
              <div className="portfolio-box-caption">
                <div className="portfolio-box-caption-content">
                  <div className="project-category text-faded">
                    Category
                  </div>
                  <div className="project-name">
                    Project Name
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div className="col-lg-4 col-sm-6">
            <a className="portfolio-box" href={require('section/assets/images/6.jpg')}>
              <img className="img-fluid" src={require('section/assets/images/6.jpg')} alt="" />
              <div className="portfolio-box-caption">
                <div className="portfolio-box-caption-content">
                  <div className="project-category text-faded">
                    Category
                  </div>
                  <div className="project-name">
                    Project Name
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>
    </section>
  );
};


export default SectionPortfolio;
