import React, { PureComponent } from 'react';
import { HashRouter } from 'react-router-dom';
import { createStore, compose } from 'redux';
import { Provider } from 'react-redux';

import Navigation from 'root/navigation';
import HeaderMainApp from 'header/header-main-app';
import Section from 'section/section';
import SectionPortfolio from 'section/section-portfolio';

import rootReducer from 'root/state';
import { APP_TITLE } from 'root/constants';
const ENV = process.env.NODE_ENV;

const boundStore = createStore.bind(this, rootReducer);
const composeEnhancer = compose(
  // Required! Enable Redux DevTools with the monitors you chose
  // DevTools.instrument()
);

function getStore() {
  if (process.env.NODE_ENV === 'development') {
    return boundStore(composeEnhancer);
  }

  return boundStore();
}

const store = getStore();

class App extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <HashRouter>
        <Provider store={ store }>
          <React.Fragment>
            <Navigation
              appTitle={APP_TITLE}
              environment={ENV} />
            <HeaderMainApp />
            <Section />
            <SectionPortfolio />
          </React.Fragment>
        </Provider>
      </HashRouter>
    );
  }
}

export default App;
