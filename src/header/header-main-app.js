'use strict';

import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Loadable from 'react-loadable';

import { MAP_TARGET_ID } from 'root/constants';
import routes from 'root/routes';


const renderMergedProps = (component, ...rest) => {
  const finalProps = Object.assign({}, ...rest);
  return (
    React.createElement(component, finalProps)
  );
};

const PropsRoute = ({ component, ...rest }) => {
  return (
    <Route {...rest} render={routeProps => {
      return renderMergedProps(component, routeProps, rest);
    }}/>
  );
};

const LazyRoute = (props) => {
  const component = Loadable({
    loader: props.component,
    loading: () => <div>Loading&hellip;</div>,
  });

  return <PropsRoute {...props} component={component} />;
};

function getCanvas() {
  return import(/* webpackChunkName: "canvas" */ '../world-tour/canvas-map')
    .then(module => {
      return module.default;
    })
    .catch(error => 'An error occurred while loading the component');
}

function getSvg() {
  return import(/* webpackChunkName: "svg" */ '../world-tour/svg-map')
    .then(module => {
      return module.default;
    })
    .catch(error => 'An error occurred while loading the component');
}

const HeaderMainApp = () => {
  return (
    <header
      className="masthead"
      id="map-target">
      <Switch>
        <LazyRoute
          exact path="/"
          component={getSvg}
          mapTarget={MAP_TARGET_ID}
          animate={true} />
        <LazyRoute
          path={routes.svg.url}
          component={getSvg}
          mapTarget={MAP_TARGET_ID}
          animate={true} />
        <LazyRoute
          path={routes.canvas.url}
          component={getCanvas}
          mapTarget={MAP_TARGET_ID} />
      </Switch>
    </header>
  );
};


export default HeaderMainApp;
