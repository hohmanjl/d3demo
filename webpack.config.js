const baseConfig = require('./webpack.config.base');
const path = require('path');
const webpack = require('webpack');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const DefinePlugin = require('webpack/lib/DefinePlugin');
const HotModuleReplacementPlugin = require('webpack/lib/HotModuleReplacementPlugin');

const { config, ASSETS_PATH } = baseConfig;


const webpackConfig = (env) => {
  env = env === undefined ? {} : env;
  const production = env.production !== 'false';
  const debug = env.debug === 'true';

  if (debug) {
    console.log('***** WARNING: BUILDING IN DEBUG MODE! *****');
  }

  if (!production) {
    console.log('***** WARNING: BUILDING IN DEVELOPMENT MODE! *****');
  }

  // Set environment variables
  config.plugins.push(
    new DefinePlugin({
      'process.env.NODE_ENV': production ? "'production'" : "'development'"
    })
  );

  let hashMethod = 'chunkhash';
  let min = '.min';
  let bundleAnalyzerConfig = {
    analyzerMode: 'static',
    reportFilename: '../bundleAnalyzer/bundles.html',
    generateStatsFile: true,
    statsFilename: '../bundleAnalyzer/bundles.json'
  };

  if (production) {
    Object.keys(config.entry).map((k) => {
      config.entry[`${k}.min`] = config.entry[k];

      delete config.entry[k];
    });

    config.plugins.push(
      new webpack.optimize.UglifyJsPlugin({
        include: /\.min\.js$/,
        parallel: true,
        uglifyOptions: {
          compress: true,
          mangle: true,
          sourceMap: false,
          output: {
            comments: false
          }
        },
      })
    );
  } else {
    hashMethod = 'hash';
    min = '';
    bundleAnalyzerConfig = {};

    config.plugins.push(
      new HotModuleReplacementPlugin()
    );

    // Enable routing fallback to root
    config.devServer = {
      historyApiFallback: true
    };
  }

  config.output = {
    path: path.join(__dirname, 'build'),
    filename: `${ASSETS_PATH}[${hashMethod}].[name].js`,
    chunkFilename: `${ASSETS_PATH}[${hashMethod}].[name]${min}.js`,
    publicPath: '/'
  };

  const commonChunkConfigs = [
    {
      filename: `${ASSETS_PATH}[${hashMethod}].[name].js`,
      minChunks: (module) => {
        return module.context && module.context.includes("node_modules");
      },
      name: `vendors${min}`,
      chunks: [`vendors${min}`, `index${min}`, `resources${min}`]
    },
    {
      filename: `${ASSETS_PATH}[${hashMethod}].[name].js`,
      minChunks: Infinity,
      name: `resources${min}`,
      chunks: [`resources${min}`, `index${min}`]
    },
    {
      filename: `${ASSETS_PATH}[${hashMethod}].[name].js`,
      name: `manifest${min}`,
      minChunks: Infinity
    }
  ];

  commonChunkConfigs.forEach((conf) => {
    config.plugins.push(
      new webpack.optimize.CommonsChunkPlugin(conf)
    );
  });

  config.plugins.push(
    new BundleAnalyzerPlugin(bundleAnalyzerConfig)
  );

  if (debug) {
    config.devtool = 'source-map';
  }

  if (production) {
    // ensure sourcemaps disabled
    if (config.devtool) {
      throw new Error('Sourcemaps forbidden in production!');
    }
  }

  return config;
};


module.exports = webpackConfig;
